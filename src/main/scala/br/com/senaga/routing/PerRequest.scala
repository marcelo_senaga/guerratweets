package br.com.senaga.routing

import akka.actor.SupervisorStrategy.Stop
import akka.actor._
import br.com.senaga.domain.{Error, RequestMessage, ResultDataMessage}
import br.com.senaga.routing.PerRequest.WithProps
import org.json4s.DefaultFormats
import spray.http.{StatusCode, StatusCodes}
import spray.httpx.Json4sSupport
import spray.routing.RequestContext

/**
 * Created by senaga on 05/09/15.
 */
trait PerRequest extends Actor with Json4sSupport {

  import context._

  val json4sFormats = DefaultFormats

  def r: RequestContext
  def target: ActorRef
  def message: RequestMessage

  target ! message

  def receive = {
    case ResultDataMessage(result) => {
      complete(StatusCodes.OK, result)
    }
  }

  def complete[T <: AnyRef](status: StatusCode, obj: T) = {
    r.complete(status, obj)
    stop(self)
  }

  override val supervisorStrategy =
    OneForOneStrategy() {
      case e => {
        complete(StatusCodes.InternalServerError, Error(e.getMessage))
        Stop
      }
    }
}

object PerRequest {
  case class WithActorRef(r: RequestContext, target: ActorRef, message: RequestMessage) extends PerRequest

  case class WithProps(r: RequestContext, props: Props, message: RequestMessage) extends PerRequest {
    lazy val target = context.actorOf(props)
  }
}

trait PerRequestCreator {
  def perRequest(actorRefFactory: ActorRefFactory, r: RequestContext, props: Props, message: RequestMessage) =
    actorRefFactory.actorOf(Props(new WithProps(r, props, message)))
}
