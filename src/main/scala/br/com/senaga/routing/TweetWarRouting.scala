package br.com.senaga.routing

import akka.actor.{Actor, Props}
import br.com.senaga.actor.FilterTweetsActor
import br.com.senaga.domain.{Tempo, InputDataMessage}
import org.json4s.DefaultFormats
import spray.http.MediaTypes._
import spray.httpx.Json4sSupport
import spray.routing._

/**
 * Created by senaga on 05/09/15.
 */
class TweetWarActor extends Actor with TweetWarService {

  def actorRefFactory = context

  def receive = runRoute(myRoute)
}

trait TweetWarService extends HttpService with PerRequestCreator with Json4sSupport {

  implicit def executionContext = actorRefFactory.dispatcher

  val json4sFormats = DefaultFormats

  import br.com.senaga.json.MessageJsonProtocol._

  val myRoute =
    pathPrefix("api") {
      path("tweets") {
        post {
          respondWithMediaType(`application/json`) {
            entity(as[Tempo]) { input =>
              monitoringTweets(input)
            }
          }
        }
      }
    }

  def monitoringTweets(input: Tempo): Route = {
    ctx => perRequest(actorRefFactory, ctx, Props[FilterTweetsActor], InputDataMessage(input))
  }
}