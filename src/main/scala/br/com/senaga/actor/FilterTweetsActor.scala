package br.com.senaga.actor

import java.io.IOException
import java.util.concurrent.TimeUnit

import akka.actor.SupervisorStrategy.Restart
import akka.actor._
import akka.routing.RoundRobinPool
import br.com.senaga.actor.FilterMessages.{FinishJob, StartMonitoring, StopMonitoring, StopWorkers}
import br.com.senaga.domain._
import br.com.senaga.twitter.TwitterStreaming

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration

/**
 * Created by senaga on 05/09/15.
 */
object FilterMessages {
  case class StartMonitoring(cidade: Cidade)
  case class StopMonitoring(cidade: Cidade)
  case class FinishJob(cidade: Cidade, errors: Int)
  case object StopWorkers
}

class FilterTweetsActor extends Actor with ActorLogging {

  val RIO_DE_JANEIRO = "RIO"
  val BELO_HORIZONTE = "BH"

  // Coordenadas obtidas a partir de:
  // https://maps.googleapis.com/maps/api/geocode/json?address=Belo+Horizonte&region=br
  // https://maps.googleapis.com/maps/api/geocode/json?address=Rio+De+Janeiro&region=br
  val cidades = List(Cidade(RIO_DE_JANEIRO,-43.7950599,-23.0763469,-43.1016081,-22.7461201),
                     Cidade(BELO_HORIZONTE, -44.0633132,-20.0291177,-43.8647871,-19.7763149))

  val router = context.actorOf(Props(new FilterByCityActor).
    withRouter(RoundRobinPool(cidades.length, supervisorStrategy = OneForOneStrategy(maxNrOfRetries = 2) {
      case _: IOException => Restart
    })), name = "router")

  var target: ActorRef = _

  val resultMap = mutable.Map[String, Int]()

  def createResultado() = {
    val errorsRio = resultMap(RIO_DE_JANEIRO)
    val errorsBH = resultMap(BELO_HORIZONTE)
    val winner = if(errorsRio < errorsBH) RIO_DE_JANEIRO else BELO_HORIZONTE
    Resultado(errorsBH, errorsRio, winner)
  }

  def receive: Receive = {
    case InputDataMessage(input) =>
      cidades.foreach(router ! StartMonitoring(_))
      context.system.scheduler.scheduleOnce(Duration.create(input.time, TimeUnit.SECONDS), self, StopWorkers)
      target = sender
      context.become(receiveFromWorkers)
  }

  def receiveFromWorkers: Receive = {
    case StopWorkers =>
      cidades.foreach(router ! StopMonitoring(_))
    case FinishJob(cidade, errors) =>
      resultMap += (cidade.name -> errors)
      if(resultMap.size >= cidades.length) {
        log.info("Finished Work!")
        target ! ResultDataMessage(createResultado())
        self ! PoisonPill
      }
  }
}

class FilterByCityActor extends Actor with ActorLogging with TwitterStreaming {
  def receive = {
    case StartMonitoring(cidade) =>
      startMonitoring(cidade)
    case StopMonitoring(cidade) =>
      stopMonitoring()
      sender ! FinishJob(cidade, errorsCount)
  }
}
