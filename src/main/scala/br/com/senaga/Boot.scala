package br.com.senaga

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import br.com.senaga.dictionary.{DictionaryFactory, DictionaryBloomFilter}
import br.com.senaga.routing.TweetWarActor
import spray.can.Http
import scala.concurrent.duration._

object Boot extends App {

  DictionaryFactory().init

  implicit val system = ActorSystem("on-spray-can")

  val service = system.actorOf(Props[TweetWarActor], "guerra-tweets")

  implicit val timeout = Timeout(4.seconds)
  IO(Http) ? Http.Bind(service, interface = "localhost", port = 8080)
}
