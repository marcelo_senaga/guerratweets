package br.com.senaga.json

import br.com.senaga.domain.{Resultado, Tempo}
import spray.json._

/**
 * Created by senaga on 05/09/15.
 */
object MessageJsonProtocol extends DefaultJsonProtocol {
  implicit val tempoInputFormat = jsonFormat1(Tempo)
  implicit val resultadoOutputFormat = jsonFormat3(Resultado)
}