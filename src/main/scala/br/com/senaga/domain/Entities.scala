package br.com.senaga.domain

/**
 * Created by senaga on 06/09/15.
 */
case class Tempo(time: Int)
case class Resultado(bh: Int, rio: Int, winner: String)

case class Cidade(name: String, lng1: Double, lat1: Double, lng2: Double, lat2: Double) {
  def getValues = List(Array(lng1, lat1), Array(lng2, lat2))
}