package br.com.senaga.domain

/**
 * Created by senaga on 05/09/15.
 */
trait RequestMessage
case class InputDataMessage(tempo: Tempo) extends RequestMessage

trait ResultMessage
case class ResultDataMessage(resultado: Resultado) extends ResultMessage

case class Error(message: String)
