package br.com.senaga.twitter

import akka.actor.{Actor, ActorLogging}
import br.com.senaga.dictionary.DictionaryFactory
import br.com.senaga.domain.Cidade
import twitter4j._

/**
 * Created by senaga on 05/09/15.
 */
trait TwitterStreaming { this: Actor with ActorLogging =>

  private val twitterStream = new TwitterStreamFactory(TwitterConfig.config()).getInstance

  var errorsCount = 0  // Seguro pois sera mesclado a um Ator

  def simpleStatusListener = new StatusListener() {
    def onStatus(status: Status) {
      log.info(s"${status.getText}")
      errorsCount += DictionaryFactory().countWrongWords(status.getText)
    }

    def onDeletionNotice(statusDeletionNotice: StatusDeletionNotice) { }
    def onTrackLimitationNotice(numberOfLimitedStatuses: Int) {  }
    def onException(ex: Exception) {  }
    def onScrubGeo(arg0: Long, arg1: Long) {  }
    def onStallWarning(warning: StallWarning) {  }
  }

  def startMonitoring(cidade: Cidade) = {
    twitterStream.addListener(simpleStatusListener)
    twitterStream.filter(new FilterQuery().locations(cidade.getValues:_*))
  }

  def stopMonitoring() = {
    twitterStream.cleanUp
    twitterStream.shutdown
  }
}
