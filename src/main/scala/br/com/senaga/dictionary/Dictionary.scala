package br.com.senaga.dictionary

/**
 * Created by senaga on 07/09/15.
 */
trait Dictionary {

  val FILENAME = "/usr/share/dict/portuguese"

  def countWrongWords(text: String): Int

  def init: Unit

  protected def loadFile(filename: String): Array[String] = {
    val source = scala.io.Source.fromFile(filename, "iso-8859-1")
    try {
      source.getLines().toArray
    } finally {
      source.close()
    }
  }
}
