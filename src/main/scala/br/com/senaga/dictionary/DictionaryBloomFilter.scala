package br.com.senaga.dictionary

import com.twitter.algebird.{BF, BloomFilter}

/**
 * Created by senaga on 07/09/15.
 */
private object DictionaryBloomFilter extends Dictionary {

  private val PROBABILITY = 0.01

  println("Creating bloom-filter...")

  private val bloom: BF = createBloomFilter(PROBABILITY, loadFile(FILENAME))

  // Utilizando bloom-filter: https://en.wikipedia.org/wiki/Bloom_filter
  private def createBloomFilter(prob: Double, words: Array[String]): BF = {
    val bloom = BloomFilter(words.length, prob)
    bloom.create(words:_*)
  }

  def countWrongWords(text: String): Int = {
    text.split("\\s+").filterNot(bloom.contains(_).isTrue).length
  }

  def init = println("...Finish")
}
