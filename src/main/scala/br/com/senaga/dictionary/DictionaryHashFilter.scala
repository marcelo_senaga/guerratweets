package br.com.senaga.dictionary

import scala.collection.mutable

/**
 * Created by senaga on 07/09/15.
 */
private object DictionaryHashFilter extends Dictionary {

  private val dictionarySet: mutable.Set[String] = createHashInMemory(loadFile(FILENAME))

  private def createHashInMemory(words: Array[String]): mutable.Set[String] = {
    val hash = mutable.HashSet[String]()
    words.foreach(hash.add(_))
    hash
  }

  def countWrongWords(text: String): Int = {
    text.split("\\s+").filterNot(dictionarySet.contains(_)).length
  }

  def init = println("...Finish")
}
