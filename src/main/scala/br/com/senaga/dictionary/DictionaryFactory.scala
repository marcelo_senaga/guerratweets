package br.com.senaga.dictionary

/**
 * Created by senaga on 07/09/15.
 */
object DictionaryFactory {

  def apply(name: String): Dictionary = name.toUpperCase match {
    case "HASH_SET" => DictionaryHashFilter
    case _ => DictionaryBloomFilter
  }

  // Default: Hash
  def apply(): Dictionary = DictionaryHashFilter
}
