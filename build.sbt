organization  := "com.example"

version       := "0.1"

scalaVersion  := "2.11.7"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

libraryDependencies ++= {

  val akkaV = "2.3.9"
  val sprayV = "1.3.3"

  Seq(
    "io.spray"            %%  "spray-can"        % sprayV,
    "io.spray"            %%  "spray-routing"    % sprayV,
    "io.spray"            %%  "spray-testkit"    % sprayV  % "test",
    "com.typesafe.akka"   %%  "akka-actor"       % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"     % akkaV   % "test",
    "org.specs2"          %%  "specs2-core"      % "2.3.11" % "test",
    "io.spray"            %%  "spray-json"       % "1.3.2",
    "org.json4s"          %% "json4s-native"     % "3.2.11",
    "org.twitter4j"       % "twitter4j-core"     % "4.0.4",
    "org.twitter4j"       % "twitter4j-stream"   % "4.0.4",
    "com.twitter"         %% "algebird-core"     % "0.11.0"
  )
}


