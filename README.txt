Exercício 3: Guerra de Tweets

Utilizando:
SBT
Scala - 2.11.7
Akka Framework / Atores
Spray
Json
twitter4j
algebird-core

Para rodar:
sbt run

Comando para fazer uma consulta:

curl -H "Content-Type: application/json" -X POST -d '{"time":60}' -m 1000 --connect-timeout 1000 http://localhost:8080/api/tweets

